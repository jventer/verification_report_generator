mod types;
use askama::Template;
use fs_err as fs;
use std::{path::PathBuf, str::FromStr};

use anyhow::Result;
use clap::Parser;
use types::{CommandLineArgs, Mermaid};

use crate::types::ReportBuilder;

fn main() -> Result<()> {
    let args = CommandLineArgs::parse();

    let full_report = ReportBuilder::new()
        .parse_cucumber_results(&args)?
        .parse_pytest_results(&args)?
        .parse_pod_logs(&args)?
        .parse_event_logs(&args)?
        .load_pytest_logs_per_scenario()?
        .load_log_event_entries_per_scenario(&args)?
        .load_env_file_info(&args)?
        .load_sequence_paths(&args)?
        .build();

    let out_file_path: PathBuf = match args.output_path {
        Some(path) => path,
        None => {
            let out_path: PathBuf = [std::env::current_dir()?, PathBuf::from_str("report.html")?]
                .iter()
                .collect();
            out_path
        }
    };
    fs::write(&out_file_path, &full_report.render()?).expect("Unable to write file");
    println!("Wrote report to {:?}", &out_file_path);

    if let Some(sequence_path) = &args.sequences_path {
        fs::create_dir_all(sequence_path.as_path())?;
        for feature in full_report.cucumber_report {
            for scenario in feature.scenarios {
                let mut file_path = sequence_path.clone();
                file_path.push(format! {"{}.html", scenario.name.replace(" ", "_")});
                let mermaid: Mermaid = scenario.into();
                fs::write(&file_path, &mermaid.render()?)
                    .expect(format!("Unable to write file {:?}", file_path.as_path()).as_str());
            }
        }
        println!("Wrote sequence diagrams to {:?}", sequence_path);
    }

    Ok(())
}
