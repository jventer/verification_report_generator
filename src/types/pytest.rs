use serde_derive::Deserialize;
use serde_derive::Serialize;
use serde_json::Value;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PytestReport {
    pub created: f64,
    pub duration: f64,
    pub exitcode: i64,
    pub root: String,
    pub environment: Environment,
    pub summary: Summary,
    pub collectors: Vec<Collector>,
    pub tests: Vec<Test>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Environment {}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Summary {
    #[serde(default)]
    pub passed: i64,
    #[serde(default)]
    pub xfailed: i64,
    // Not sure why skipped is missed
    #[serde(default)]
    pub skipped: i64,
    #[serde(default)]
    pub total: i64,
    #[serde(default)]
    pub collected: i64,
    #[serde(default)]
    pub deselected: i64,
    #[serde(default)]
    pub failed: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Collector {
    pub nodeid: String,
    pub outcome: String,
    pub result: Vec<Result>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Result {
    pub nodeid: String,
    #[serde(rename = "type")]
    pub type_field: String,
    pub lineno: Option<i64>,
    pub deselected: Option<bool>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Test {
    pub nodeid: String,
    pub lineno: i64,
    pub outcome: String,
    pub keywords: Vec<String>,
    pub setup: Setup,
    pub call: Option<Call>,
    pub teardown: Teardown,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Setup {
    pub duration: f64,
    pub outcome: String,
    #[serde(default)]
    pub log: Vec<SetupLog>,
    pub longrepr: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetupLog {
    pub name: String,
    pub msg: String,
    pub args: Value,
    pub levelname: String,
    #[serde(default)]
    pub levelno: i64,
    pub pathname: String,
    pub filename: String,
    pub module: String,
    #[serde(rename = "exc_info")]
    pub exc_info: Value,
    #[serde(rename = "exc_text")]
    pub exc_text: Value,
    #[serde(rename = "stack_info")]
    pub stack_info: Value,
    #[serde(default)]
    pub lineno: i64,
    pub func_name: String,
    #[serde(default)]
    pub created: f64,
    #[serde(default)]
    pub msecs: f64,
    #[serde(default)]
    pub relative_created: f64,
    #[serde(default)]
    pub thread: i64,
    pub thread_name: String,
    pub process_name: String,
    #[serde(default)]
    pub process: i64,
    pub asctime: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Call {
    #[serde(default)]
    pub duration: f64,
    #[serde(default)]
    pub outcome: String,
    #[serde(default)]
    pub log: Vec<CallLog>,
    #[serde(default)]
    pub longrepr: Option<String>,
    #[serde(default)]
    pub stdout: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CallLog {
    pub name: String,
    pub msg: String,
    pub args: Value,
    pub levelname: String,
    pub levelno: i64,
    pub pathname: String,
    pub filename: String,
    pub module: String,
    #[serde(rename = "exc_info")]
    pub exc_info: Value,
    #[serde(rename = "exc_text")]
    pub exc_text: Value,
    #[serde(rename = "stack_info")]
    pub stack_info: Value,
    pub lineno: i64,
    pub func_name: String,
    pub created: f64,
    pub msecs: f64,
    pub relative_created: f64,
    pub thread: i64,
    pub thread_name: String,
    pub process_name: String,
    pub process: i64,
    pub asctime: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Teardown {
    pub duration: f64,
    pub outcome: String,
}
