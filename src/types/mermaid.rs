use askama::Template;

use super::Scenario;

#[derive(Debug, Clone)]
pub struct MermaidLine {
    mermaid_line: String,
}

impl MermaidLine {
    pub fn get_parent(device_name: &String) -> Option<String> {
        match device_name.as_str() {
            "DishManager" => Some("PyTest".to_string()),
            "DSManager" => Some("DishManager".to_string()),
            "SPFDevice" => Some("DishManager".to_string()),
            "SPFRxDevice" => Some("DishManager".to_string()),
            _ => None,
        }
    }

    pub fn build_event_line(
        device_name: &String,
        attribute_name: &Option<String>,
        attribute_value: &Option<String>,
    ) -> Option<String> {
        if let (Some(attr_name), Some(attr_value)) = (attribute_name, attribute_value) {
            // Note over DishManager:

            // return Some(format!(
            //     "{device_name}-->{device_name}: {attr_name} : {attr_value}"
            // ));
            return Some(format!(
                "Note over {device_name}: {attr_name} : {attr_value}"
            ));
        }
        None
    }

    pub fn build_command_line(
        device_name: &String,
        command_in: &Option<String>,
        command_result: &Option<String>,
    ) -> Option<String> {
        let parent = MermaidLine::get_parent(&device_name)?;

        if let Some(comm_name) = command_in {
            let parts: Vec<&str> = comm_name.split(".").collect();
            let comm = parts.get(1)?;
            return Some(format!(
                "{parent}->>{device_name}: {parent} - {comm} - {device_name}"
            ));
        }
        if let Some(comm_res) = command_result {
            return Some(format!(
                "{device_name}-->>{parent}: {parent} - {comm_res} - {device_name}, "
            ));
        }
        None
    }
}

#[derive(Default, Debug, Clone, Template)]
#[template(path = "mermaid.html")]
pub struct Mermaid {
    name: String,
    sequence_entries: Vec<MermaidLine>,
}

impl Mermaid {
    pub fn collapse_sequence_entries(&self) -> String {
        self.sequence_entries
            .iter()
            .map(|s| s.mermaid_line.clone())
            .collect::<Vec<String>>()
            .join("\n")
    }
}

impl From<Scenario> for Mermaid {
    fn from(scenario: Scenario) -> Self {
        let mut sequence_entries: Vec<MermaidLine> = Vec::new();
        for log_entry in scenario.log_entries {
            if log_entry.command_in.is_some() {
                if let Some(mermaid_line) = MermaidLine::build_command_line(
                    &log_entry.device_name,
                    &log_entry.command_in,
                    &log_entry.command_result,
                ) {
                    sequence_entries.push(MermaidLine { mermaid_line });
                }
            }
            if log_entry.command_result.is_some() {
                if let Some(mermaid_line) = MermaidLine::build_command_line(
                    &log_entry.device_name,
                    &log_entry.command_in,
                    &log_entry.command_result,
                ) {
                    sequence_entries.push(MermaidLine { mermaid_line });
                }
            }
            if log_entry.event_attr_name.is_some() {
                if let Some(mermaid_line) = MermaidLine::build_event_line(
                    &log_entry.device_name,
                    &log_entry.event_attr_name,
                    &log_entry.event_attr_value,
                ) {
                    sequence_entries.push(MermaidLine { mermaid_line });
                }
            }
        }
        Self {
            name: scenario.name.clone(),
            sequence_entries,
        }
    }
}
