use anyhow::anyhow;
use anyhow::Result;
use fs_err::{self as fs, read_dir, DirEntry};
use jiff::Timestamp;
use regex::Regex;
use std::{collections::BTreeMap, path::Path};

#[derive(Default, Debug, Clone, PartialEq)]
pub struct LogEntry {
    pub log_line: String,
    pub device_name: String, // E.g DishManager
    pub time_stamp: Timestamp,
    pub command_in: Option<String>,
    pub command_result: Option<String>,
    pub event_attr_name: Option<String>,
    pub event_attr_value: Option<String>,
}

impl LogEntry {
    //Event	2024-08-26 13:20:12.194850	DishManager(mid-dish/dish-manager/ska001)	b4capabilitystate	STANDBY
    pub fn try_from_event_line(log_str: &str, event_regex: &Regex) -> Result<LogEntry> {
        let mut log_entry = LogEntry::default();
        log_entry.log_line = log_str.to_string();

        if log_str.starts_with("Event") {
            let parts: Vec<&str> = log_str.split("\t").collect();
            // time
            if let Some(t_stamp) = parts.get(1) {
                let mut t_stamp = t_stamp.replace(" ", "T");
                t_stamp.push('Z');
                log_entry.time_stamp = t_stamp.parse()?;
            }

            if let Some(attr_name) = parts.get(3) {
                log_entry.event_attr_name = Some(attr_name.to_string());
            }

            if let Some(attr_value) = parts.get(4) {
                log_entry.event_attr_value = Some(attr_value.to_string());
            }

            if let Some(device_name) = parts.get(2) {
                if let Some(caps) = event_regex.captures(device_name) {
                    log_entry.device_name = caps
                        .get(1)
                        .map_or("".to_string(), |m| m.as_str().to_string());
                }
            }
            return Ok(log_entry);
        }

        Err(anyhow!("Could not parse line"))
    }

    // 1|2024-08-26T13:18:32.053Z|INFO|Dummy-11|_info_patch|base_device.py#437|tango-device:mid-dish/dish-manager/SKA001|-> DishManager.SetStandbyFPMode()
    // 1|2024-08-26T13:18:32.054Z|INFO|Dummy-11|_info_patch|base_device.py#437|tango-device:mid-dish/dish-manager/SKA001|([<ResultCode.QUEUED: 2>], ['1724678312.0537553_76933844560734_SetStandbyFPMode']) <- DishManager.SetStandbyFPMode()
    // 1|2024-08-26T13:18:32.020Z|DEBUG|Dummy-11|_update_component_state|dish_manager_cm.py#555|tango-device:mid-dish/dish-manager/SKA001|Updating dish manager component state with [{'b2capabilitystate': <CapabilityStates.STANDBY: 1>}]
    pub fn try_from_pod_line(
        log_str: &str,
        command_in_regex: &Regex,
        command_out_regex: &Regex,
    ) -> Result<LogEntry> {
        let mut log_entry = LogEntry::default();
        log_entry.log_line = log_str.to_string();

        if log_str.starts_with("1|") {
            let parts: Vec<&str> = log_str.split('|').collect();

            // time
            if let Some(t_stamp) = parts.get(1) {
                log_entry.time_stamp = t_stamp.parse()?;
            }

            // Command
            if let Some(log_msg) = parts.get(7) {
                // -> DishManager.SetStandbyFPMode()
                if log_msg.contains("->") {
                    if let Some(caps) = command_in_regex.captures(log_msg) {
                        let device_name = caps
                            .get(1)
                            .map_or("".to_string(), |m| m.as_str().to_string());
                        let command_name = caps
                            .get(2)
                            .map_or("".to_string(), |m| m.as_str().to_string());
                        let params = caps
                            .get(3)
                            .map_or("".to_string(), |m| m.as_str().to_string());
                        log_entry.device_name = device_name.clone();
                        log_entry.command_in =
                            Some(format!("{device_name}.{command_name}({params})"));
                    }
                }

                // ([<ResultCode.QUEUED: 2>], ['1724678312.0537553_76933844560734_SetStandbyFPMode']) <- DishManager.SetStandbyFPMode()
                if log_msg.contains("<-") {
                    if let Some(caps) = command_out_regex.captures(log_msg) {
                        let result = caps
                            .get(1)
                            .map_or("".to_string(), |m| m.as_str().to_string());
                        let device_name = caps
                            .get(2)
                            .map_or("".to_string(), |m| m.as_str().to_string());
                        log_entry.device_name = device_name.clone();
                        log_entry.command_result = Some(result);
                    }
                }
            }
            return Ok(log_entry);
        }
        return Err(anyhow!("Could not parse line"));
    }
}

#[derive(Debug)]
pub struct PodEventLogs {
    pub log_entries: BTreeMap<Timestamp, LogEntry>,
    pub command_in_regex: Regex,
    pub command_out_regex: Regex,
    pub device_name_regex: Regex, // Pull DishManager from DishManager(mid-dish/dish-manager/ska001)
}

impl Default for PodEventLogs {
    fn default() -> Self {
        Self {
            log_entries: Default::default(),
            command_in_regex: Regex::new(r"\w*->\s(\w*).(\w*)\((.*)\)").unwrap(),
            command_out_regex: Regex::new(r"(.*)\s<-\s(\w*).(.*)").unwrap(),
            device_name_regex: Regex::new(r"(\w*)\(.*").unwrap(),
        }
    }
}

impl PodEventLogs {
    fn parse_log_file(&self, dir_entry: &DirEntry) -> Vec<LogEntry> {
        let mut logs = Vec::new();
        if let Ok(file_strings) = fs::read_to_string(dir_entry.path()) {
            for line in file_strings.lines() {
                if let Ok(log_entry) = LogEntry::try_from_pod_line(
                    line,
                    &self.command_in_regex,
                    &self.command_out_regex,
                ) {
                    logs.push(log_entry);
                }
            }
        }
        logs
    }

    fn parse_events_file(&self, dir_entry: &DirEntry) -> Vec<LogEntry> {
        let mut logs = Vec::new();
        if let Ok(file_strings) = fs::read_to_string(dir_entry.path()) {
            for line in file_strings.lines() {
                if let Ok(log_entry) = LogEntry::try_from_event_line(line, &self.device_name_regex)
                {
                    logs.push(log_entry);
                }
            }
        }
        logs
    }

    pub fn parse_logs_dir(&mut self, logs_dir_path: &Path) {
        if let Ok(readdir) = read_dir(logs_dir_path) {
            for a in readdir.into_iter().filter_map(|f| f.ok()) {
                for log_entry in self.parse_log_file(&a).into_iter() {
                    self.log_entries.insert(log_entry.time_stamp, log_entry);
                }
            }
        }
    }

    pub fn parse_events_dir(&mut self, logs_dir_path: &Path) {
        if let Ok(readdir) = read_dir(logs_dir_path) {
            for a in readdir.into_iter().filter_map(|f| f.ok()) {
                for log_entry in self.parse_events_file(&a).into_iter() {
                    self.log_entries.insert(log_entry.time_stamp, log_entry);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use crate::types::{LogEntry, PodEventLogs};
    use fs_err::read_dir;
    use jiff::Timestamp;

    #[test]
    fn test_log_event() -> anyhow::Result<(), anyhow::Error> {
        let pod_logs = PodEventLogs::default();
        let logline = "Event	2024-08-26 13:20:12.194850	DishManager(mid-dish/dish-manager/ska001)	b4capabilitystate	STANDBY";
        let left: LogEntry = LogEntry::try_from_event_line(logline, &pod_logs.device_name_regex)?;
        let expected_time: Timestamp = "2024-08-26T13:20:12.194850Z".parse()?;
        assert_eq!(left.time_stamp, expected_time);
        assert_eq!(left.log_line, logline);
        assert_eq!(left.device_name, "DishManager".to_string());
        assert_eq!(left.event_attr_name, Some("b4capabilitystate".to_string()));
        assert_eq!(left.event_attr_value, Some("STANDBY".to_string()));
        Ok(())
    }

    #[test]
    fn test_log_event_lrc() -> anyhow::Result<(), anyhow::Error> {
        let pod_logs = PodEventLogs::default();
        let logline = "Event	2024-08-26 13:19:23.729105	DishManager(mid-dish/dish-manager/ska001)	longrunningcommandprogress	('1724678363.6949368_267217221037276_Track', 'Track called on DS, ID 1724678363.6954203_259181993646097_DS_Track')";
        let left: LogEntry = LogEntry::try_from_event_line(logline, &pod_logs.device_name_regex)?;
        let expected_time: Timestamp = "2024-08-26T13:19:23.729105Z".parse()?;
        assert_eq!(left.time_stamp, expected_time);
        assert_eq!(left.log_line, logline);
        assert_eq!(left.device_name, "DishManager".to_string());
        assert_eq!(
            left.event_attr_name,
            Some("longrunningcommandprogress".to_string())
        );
        assert_eq!(left.event_attr_value, Some("('1724678363.6949368_267217221037276_Track', 'Track called on DS, ID 1724678363.6954203_259181993646097_DS_Track')".to_string()));
        Ok(())
    }

    #[test]
    fn test_log_line_general() -> anyhow::Result<(), anyhow::Error> {
        let pod_logs = PodEventLogs::default();
        let logline = "1|2024-08-21T13:37:17.461Z|INFO|MainThread|exit|core.py#137||Finished processing state _UNINITIALISED exit callbacks.";
        let left: LogEntry = LogEntry::try_from_pod_line(
            logline,
            &pod_logs.command_in_regex,
            &pod_logs.command_out_regex,
        )?;
        let expected_time: Timestamp = "2024-08-21T13:37:17.461Z".parse()?;
        assert_eq!(left.time_stamp, expected_time);
        assert_eq!(left.log_line, logline);
        Ok(())
    }

    #[test]
    fn test_log_line_command_in() -> anyhow::Result<(), anyhow::Error> {
        let pod_logs = PodEventLogs::default();
        let logline = "1|2024-08-21T13:37:38.685Z|INFO|Dummy-11|_info_patch|base_device.py#437|tango-device:mid-dish/dish-manager/SKA001|-> DishManager.SetStandbyFPMode(T)";
        let left: LogEntry = LogEntry::try_from_pod_line(
            logline,
            &pod_logs.command_in_regex,
            &pod_logs.command_out_regex,
        )?;
        let expected_time: Timestamp = "2024-08-21T13:37:38.685Z".parse()?;
        assert_eq!(left.time_stamp, expected_time);
        assert_eq!(left.log_line, logline);
        assert_eq!(
            left.command_in,
            Some("DishManager.SetStandbyFPMode(T)".to_string())
        );
        assert_eq!(left.device_name, "DishManager".to_string());
        Ok(())
    }

    #[test]
    fn test_log_line_command_out() -> anyhow::Result<(), anyhow::Error> {
        let pod_logs = PodEventLogs::default();
        let logline = "1|2024-08-26T13:18:32.054Z|INFO|Dummy-11|_info_patch|base_device.py#437|tango-device:mid-dish/dish-manager/SKA001|([<ResultCode.QUEUED: 2>], ['1724678312.0537553_76933844560734_SetStandbyFPMode']) <- DishManager.SetStandbyFPMode()";
        let left: LogEntry = LogEntry::try_from_pod_line(
            logline,
            &pod_logs.command_in_regex,
            &pod_logs.command_out_regex,
        )?;
        let expected_time: Timestamp = "2024-08-26T13:18:32.054Z".parse()?;
        assert_eq!(left.time_stamp, expected_time);
        assert_eq!(left.log_line, logline);
        assert_eq!(
            left.command_result,
            Some("([<ResultCode.QUEUED: 2>], ['1724678312.0537553_76933844560734_SetStandbyFPMode'])".to_string())
        );
        assert_eq!(left.device_name, "DishManager".to_string());
        Ok(())
    }

    #[test]
    fn test_parse_files() -> anyhow::Result<(), anyhow::Error> {
        let mut dir = read_dir("./build/logs")?;
        let single_file = dir.nth(0).unwrap()?;
        let pod_logs = PodEventLogs::default();
        let log_lines = pod_logs.parse_log_file(&single_file);
        assert!(log_lines.len() > 0);
        Ok(())
    }

    #[test]
    fn test_parse_logs_dir() -> anyhow::Result<(), anyhow::Error> {
        let path = PathBuf::from("./build/logs");
        let mut pod_logs = PodEventLogs::default();
        pod_logs.parse_logs_dir(&path);
        assert!(pod_logs.log_entries.len() > 0);
        Ok(())
    }
}
