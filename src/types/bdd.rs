use anyhow::Result;

use askama::Template;

use chrono::prelude::*;
use serde_derive::Deserialize;
use serde_derive::Serialize;

use std::collections::BTreeMap;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;
use std::str::FromStr;
use std::time::Duration;
use std::time::UNIX_EPOCH;
use uuid::Uuid;

use super::event_and_pod_logs;
use super::CallLog;
use super::CommandLineArgs;
use super::FullReport;
use super::LogEntry;
use super::PodEventLogs;
use super::PytestReport;
use super::SetupLog;

pub type CucumberReport = Vec<Feature>;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize, Template)]
#[serde(rename_all = "camelCase")]
#[template(path = "feature.html")]

// #[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
// #[serde(rename_all = "camelCase")]
pub struct Feature {
    pub keyword: String,
    pub uri: String,
    pub name: String,
    pub id: String,
    pub line: i64,
    pub description: String,
    pub tags: Vec<FeatureTag>,
    #[serde(rename = "elements")]
    pub scenarios: Vec<Scenario>,
}

impl Feature {
    pub fn passed(&self) -> bool {
        self.scenarios.iter().all(|sc| sc.passed())
    }

    pub fn scenarios_passed_count(&self) -> usize {
        self.scenarios
            .iter()
            .filter(|sc| sc.status() == "passed")
            .count()
    }
    pub fn scenarios_skipped_count(&self) -> usize {
        self.scenarios
            .iter()
            .filter(|sc| sc.status() == "skipped")
            .count()
    }
    pub fn scenarios_failed_count(&self) -> usize {
        self.scenarios
            .iter()
            .filter(|sc| sc.status() == "failed")
            .count()
    }

    pub fn collapse_uuid() -> String {
        Uuid::new_v4().to_string()
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FeatureTag {
    pub name: String,
    pub line: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize, Template)]
#[serde(rename_all = "camelCase")]
#[template(path = "scenario.html")]
pub struct Scenario {
    pub keyword: String,
    pub id: String,
    pub name: String,
    pub line: i64,
    pub description: String,
    pub tags: Vec<ElementTag>,
    #[serde(rename = "type")]
    pub type_field: String,
    pub steps: Vec<Step>,
    pub logs: Option<Vec<String>>,
    // pub called_commands: Option<Vec<String>>,
    // pub events: Option<Vec<String>>,
    pub start_time: Option<String>,
    pub end_time: Option<String>,
    pub elastic_url: Option<String>,
    pub commands: Option<Vec<String>>,
    #[serde(skip_deserializing, skip_serializing)]
    pub log_entries: Vec<LogEntry>,
    #[serde(skip_deserializing, skip_serializing)]
    pub sequence_path: Option<String>,
}

impl Scenario {
    pub fn load_logs_from_pytest(&mut self, pytest_report: &PytestReport) {
        let mut log_strings: Vec<String> = Vec::new();
        let mut setup_logs: Vec<SetupLog> = Vec::new();
        let mut call_logs: Vec<CallLog> = Vec::new();
        for test in pytest_report.tests.iter() {
            if test.nodeid.ends_with(self.id.as_str()) {
                for log in test.setup.log.iter() {
                    log_strings.push(format!("{:?}", log));
                    setup_logs.push(log.clone());
                }
                if let Some(call) = &test.call {
                    for log in call.log.iter() {
                        log_strings.push(format!("{:?}", log));
                        call_logs.push(log.clone());
                    }
                }
            }
        }
        if let Some(setup_log) = setup_logs.first() {
            let t: u64 = (setup_log.created * 1000.0).round() as u64;
            let d = UNIX_EPOCH + Duration::from_millis(t);
            let i = DateTime::<Utc>::from(d);
            self.start_time = Some(i.to_rfc3339_opts(SecondsFormat::Millis, true))
        }
        if let Some(call_log) = call_logs.last() {
            let t: u64 = (call_log.created * 1000.0).round() as u64;
            let d: std::time::SystemTime = UNIX_EPOCH + Duration::from_millis(t);
            let i = DateTime::<Utc>::from(d);
            self.end_time = Some(i.to_rfc3339_opts(SecondsFormat::Millis, true))
        }
        self.logs = Some(log_strings);
    }

    pub fn load_log_event_entries(&mut self, pod_event_logs: &PodEventLogs) {
        if let Some(start_time) = &self.start_time {
            if let Some(end_time) = &self.end_time {
                if let Ok(parsed_start_time) = jiff::Timestamp::from_str(start_time) {
                    if let Ok(parsed_end_time) = jiff::Timestamp::from_str(end_time) {
                        for (_ts, log_entry) in pod_event_logs
                            .log_entries
                            .range(parsed_start_time..parsed_end_time)
                        {
                            self.log_entries.push(log_entry.clone());
                        }
                    }
                }
            }
        }
    }

    pub fn passed(&self) -> bool {
        !self.steps.iter().any(|step| step.result.status == "failed")
    }

    pub fn status(&self) -> String {
        if self.steps.iter().any(|step| step.result.status == "failed") {
            return String::from("failed");
        }
        if self
            .steps
            .iter()
            .any(|step| step.result.status == "skipped")
        {
            return String::from("skipped");
        }
        String::from("passed")
    }

    pub fn passed_steps_count(&self) -> usize {
        self.steps
            .iter()
            .filter(|step| step.result.status == "passed")
            .count()
    }

    pub fn failed_steps_count(&self) -> usize {
        self.steps
            .iter()
            .filter(|step| step.result.status == "failed")
            .count()
    }

    pub fn skipped_steps_count(&self) -> usize {
        self.steps
            .iter()
            .filter(|step| step.result.status == "skipped")
            .count()
    }

    pub fn collapse_uuid() -> String {
        Uuid::new_v4().to_string()
    }

    pub fn collapse_pytest_logs(&self) -> String {
        if let Some(logs) = &self.logs {
            return logs.join("\n");
        }
        "".to_string()
    }

    pub fn collapse_event_logs(&self) -> String {
        let items: Vec<String> = self
            .log_entries
            .iter()
            .filter(|l| l.event_attr_name.is_some())
            .map(|l| l.log_line.clone())
            .collect();
        items.join("\n")
    }

    pub fn collapse_pod_logs(&self) -> String {
        let items: Vec<String> = self
            .log_entries
            .iter()
            .filter(|l| l.event_attr_name.is_none())
            .map(|l| l.log_line.clone())
            .collect();
        items.join("\n")
    }

    pub fn collapse_called_commands(&self) -> String {
        let items: Vec<String> = self
            .log_entries
            .iter()
            .filter_map(|f| f.command_in.clone())
            .collect();
        items.join("\n")
    }

    pub fn load_elastic_url(&mut self, namespace: String) {
        if let Some(start_time) = self.start_time.clone() {
            if let Some(end_time) = self.end_time.clone() {
                self.elastic_url = Some(FullReport::generate_elastic_search_url(
                    &namespace,
                    &start_time,
                    &end_time,
                ));
            }
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ElementTag {
    pub name: String,
    pub line: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Step {
    pub keyword: String,
    pub name: String,
    pub line: i64,
    #[serde(rename = "match")]
    pub match_field: Match,
    pub result: ScenarioResult,
}

impl Step {
    pub fn has_error_message(&self) -> bool {
        match &self.result.error_message {
            Some(m) => !m.is_empty(),
            None => false,
        }
    }
    pub fn get_error_message(&self) -> String {
        match &self.result.error_message {
            Some(m) => m.clone(),
            None => String::from(""),
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Match {
    pub location: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
// #[serde(rename_all = "camelCase")]
pub struct ScenarioResult {
    pub status: String,
    pub duration: i64,
    #[serde(rename = "error_message")]
    pub error_message: Option<String>,
}
