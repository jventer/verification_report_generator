use anyhow::bail;

use anyhow::Result;
use askama::Template;
use chrono::prelude::*;
use chrono::Duration;
use clap::Parser;

use fs_err as fs;

use std::{
    io::{BufRead, BufReader},
    path::PathBuf,
};

use super::Feature;
use super::PodEventLogs;
use super::{CucumberReport, LogEntry, PytestReport};

pub struct L2MapEntry {
    l2_name: String,
    name: String,
    status: String,
}

#[derive(Default, Debug)]
pub struct EnvInfo {
    branch: Option<String>,
    sha: Option<String>,
    short_sha: Option<String>,
    commit_timestamp: Option<String>,
    job_id: Option<String>,
    job_url: Option<String>,
    pipeline_id: Option<String>,
    pipeline_url: Option<String>,
    kube_namespace: Option<String>,
    pipeline_created: Option<String>,
}

#[derive(Parser, Debug)]
#[command(version, about)]
pub struct CommandLineArgs {
    #[arg(short, long, help = "Cucumber BDD results in JSON format")]
    pub cucumber_file_path: PathBuf,

    #[arg(short, long, help = "Pytest results in JSON format")]
    pub pytest_file_path: PathBuf,

    #[arg(short, long, help = "Directory that contains the events per test")]
    pub events_path: Option<PathBuf>,

    #[arg(short = 'd', long, help = "Directory that contains the pod logs")]
    pub pod_logs_path: Option<PathBuf>,

    #[arg(short, long, help = "Defaults to 'report.html'")]
    pub output_path: Option<PathBuf>,

    #[arg(short, long, help = "Directory for sequence diagrams")]
    pub sequences_path: Option<PathBuf>,

    #[arg(
        short = 'n',
        long,
        help = "Path to the environment file containing Gitlab CI information"
    )]
    pub env_file_path: Option<PathBuf>,
}

#[derive(Default)]
pub struct ReportBuilder {
    pytest_report: PytestReport,
    cucumber_report: CucumberReport,
    env_info: EnvInfo,
    pod_logs: PodEventLogs,
}

impl ReportBuilder {
    pub fn load_sequence_paths(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        if let Some(sequences_path) = &args.sequences_path {
            for feature in &mut self.cucumber_report {
                for scenario in &mut feature.scenarios {
                    let mut file_path = sequences_path.clone();
                    file_path.push(format! {"{}.html", scenario.name.replace(" ", "_")});
                    if let Ok(file_path_str) = file_path.into_os_string().into_string() {
                        scenario.sequence_path = Some(file_path_str);
                    }
                }
            }
        }
        Ok(self)
    }

    pub fn new() -> ReportBuilder {
        ReportBuilder::default()
    }

    pub fn parse_cucumber_results(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        let report_contents = fs::read_to_string(&args.cucumber_file_path)?;
        let jd = &mut serde_json::Deserializer::from_str(&report_contents);

        match serde_path_to_error::deserialize(jd) {
            Ok(res) => self.cucumber_report = res,
            Err(err) => {
                let path = err.path().to_string();
                bail!("Parse error at JSON path: {}", path);
            }
        }
        Ok(self)
    }

    pub fn parse_pytest_results(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        let report_contents = fs::read_to_string(&args.pytest_file_path)?;
        let jd = &mut serde_json::Deserializer::from_str(&report_contents);

        match serde_path_to_error::deserialize(jd) {
            Ok(res) => self.pytest_report = res,
            Err(err) => {
                let path = err.path().to_string();
                bail!("Parse error at JSON path: {}", path);
            }
        }
        Ok(self)
    }

    pub fn parse_pod_logs(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        if let Some(pod_logs_path) = &args.pod_logs_path {
            &self.pod_logs.parse_logs_dir(pod_logs_path.as_path());
        }
        Ok(self)
    }

    pub fn parse_event_logs(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        if let Some(event_logs_path) = &args.events_path {
            &self.pod_logs.parse_events_dir(event_logs_path.as_path());
        }
        Ok(self)
    }

    pub fn load_pytest_logs_per_scenario(mut self) -> Result<ReportBuilder> {
        for feature in self.cucumber_report.iter_mut() {
            for scenario in feature.scenarios.iter_mut() {
                scenario.load_logs_from_pytest(&self.pytest_report);
            }
        }
        Ok(self)
    }

    pub fn load_log_event_entries_per_scenario(
        mut self,
        args: &CommandLineArgs,
    ) -> Result<ReportBuilder> {
        for feature in self.cucumber_report.iter_mut() {
            for scenario in feature.scenarios.iter_mut() {
                let _ = scenario.load_log_event_entries(&self.pod_logs);
            }
        }
        Ok(self)
    }

    // pub fn load_pod_logs_per_scenario(mut self) -> Result<ReportBuilder> {
    //     for feature in self.cucumber_report.iter_mut() {
    //         for scenario in feature.scenarios.iter_mut() {
    //             scenario.load_pod_logs(&self.pod_logs.log_entries);
    //         }
    //     }
    //     Ok(self)
    // }

    pub fn load_env_file_info(mut self, args: &CommandLineArgs) -> Result<ReportBuilder> {
        let mut env_info: EnvInfo = EnvInfo::default();
        if let Some(file_path) = args.env_file_path.clone() {
            let opened_file = fs::File::open(file_path)?;
            let br = BufReader::new(opened_file);
            let read_lines: Vec<String> = br.lines().map_while(Result::ok).collect();
            for mut line in read_lines.into_iter().clone() {
                if let Some(ix) = line.find(':') {
                    let mut env_var_value = line.split_off(ix);
                    let env_var = env_var_value.split_off(1);

                    match line.as_str() {
                        "CI_COMMIT_BRANCH" => env_info.branch = Some(env_var),
                        "CI_COMMIT_SHA" => env_info.sha = Some(env_var),
                        "CI_COMMIT_SHORT_SHA" => env_info.short_sha = Some(env_var),
                        "CI_COMMIT_TIMESTAMP" => env_info.commit_timestamp = Some(env_var),
                        "CI_JOB_ID" => env_info.job_id = Some(env_var),
                        "CI_JOB_URL" => env_info.job_url = Some(env_var),
                        "CI_PIPELINE_ID" => env_info.pipeline_id = Some(env_var),
                        "CI_PIPELINE_URL" => env_info.pipeline_url = Some(env_var),
                        "KUBE_NAMESPACE" => env_info.kube_namespace = Some(env_var),
                        "CI_PIPELINE_CREATED_AT" => env_info.pipeline_created = Some(env_var),
                        _ => {}
                    }
                }
            }
            self.env_info = env_info;
            self = self.load_scenario_elastic_urls();
        }
        Ok(self)
    }

    pub fn load_scenario_elastic_urls(mut self) -> ReportBuilder {
        for feature in self.cucumber_report.iter_mut() {
            for scenario in feature.scenarios.iter_mut() {
                if let Some(ns) = &self.env_info.kube_namespace {
                    scenario.load_elastic_url(ns.clone());
                }
            }
        }
        self
    }

    pub fn build(self) -> FullReport {
        FullReport {
            pytest_report: self.pytest_report,
            cucumber_report: self.cucumber_report,
            env_info: self.env_info,
        }
    }
}

#[derive(Template)]
#[template(path = "base.html")]
pub struct FullReport {
    pub pytest_report: PytestReport,
    pub cucumber_report: CucumberReport,
    pub env_info: EnvInfo,
}

impl FullReport {
    fn scenarios_count(&self) -> usize {
        let mut scenarios_count: usize = 0;
        for feature in self.cucumber_report.iter() {
            scenarios_count += feature.scenarios.len();
        }
        scenarios_count
    }

    fn scenarios_passed_count(&self) -> usize {
        let mut scenarios_count: usize = 0;
        for feature in self.cucumber_report.iter() {
            for scenario in feature.scenarios.iter() {
                if scenario.status() == "passed" {
                    scenarios_count += 1;
                }
            }
        }
        scenarios_count
    }

    fn scenarios_failed_count(&self) -> usize {
        let mut scenarios_count: usize = 0;
        for feature in self.cucumber_report.iter() {
            for scenario in feature.scenarios.iter() {
                if scenario.status() == "failed" {
                    scenarios_count += 1;
                }
            }
        }
        scenarios_count
    }

    fn scenarios_skipped_count(&self) -> usize {
        let mut scenarios_count: usize = 0;
        for feature in self.cucumber_report.iter() {
            for scenario in feature.scenarios.iter() {
                if scenario.status() == "skipped" {
                    scenarios_count += 1;
                }
            }
        }
        scenarios_count
    }

    pub fn get_l2_map(&self) -> Vec<L2MapEntry> {
        let mut l2_map: Vec<L2MapEntry> = Vec::new();
        for feature in self.cucumber_report.iter() {
            for scenario in feature.scenarios.iter() {
                for tag in scenario.tags.iter() {
                    if tag.name.starts_with("L2-") {
                        l2_map.push(L2MapEntry {
                            l2_name: tag.name.clone(),
                            name: scenario.name.clone(),
                            status: scenario.status(),
                        })
                    }
                }
            }
        }
        l2_map.dedup_by_key(|i| i.name.clone());
        l2_map.sort_by(|a, b| a.l2_name.cmp(&b.l2_name));
        l2_map
    }

    pub fn generate_test_run_elastic_searh_url(&self) -> Option<String> {
        let kube_namespace: String = self.env_info.kube_namespace.clone()?;
        let start_time: String = self.env_info.pipeline_created.clone()?;
        if let Ok(dt) = DateTime::parse_from_rfc3339(&start_time) {
            let end_time = dt + Duration::hours(2);
            return Some(FullReport::generate_elastic_search_url(
                &kube_namespace,
                &dt.to_rfc3339_opts(SecondsFormat::Millis, true),
                &end_time.to_rfc3339_opts(SecondsFormat::Millis, true),
            ));
        }
        None
    }

    pub fn generate_elastic_search_url(
        namespace: &String,
        start_time: &String,
        end_time: &String,
    ) -> String {
        format!("https://k8s.stfc.skao.int/kibana/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:'{}',to:'{}'))&_a=(columns:!(),filters:!(),interval:auto,query:(language:kuery,query:'kubernetes.namespace%20:%20%22{}%22'),sort:!(!('@timestamp',desc)))", start_time, end_time, namespace)
    }
}
